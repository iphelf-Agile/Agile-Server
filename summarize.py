from dbkit import Database


def main():
    db = Database("company.db")
    key = "企业名称"
    db.key(key)
    table = "聚类结果"
    fields = [
        "企业信用-信用水平",
        "企业信用-信用危机",
        "企业稳定性-企业能力",
        "企业稳定性-稳定水平",
        "企业规模背景-企业年龄",
        "企业规模背景-企业规模",
        "企业风险-司法风险",
        "企业风险-经营风险"
    ]
    sqlSummary = "select [%s],count([%s]) from [" + table + "] group by [%s]"
    data = []
    for i in range(len(fields)):
        db.cursor.execute(sqlSummary % (fields[i], fields[i], fields[i]))
        data.append(db.cursor.fetchall())
        print(fields[i])
        total = 0
        for value in data[i]:
            if value[0] is not None:
                print(str(value[0]) + "\t" + str(value[1]))
                total += int(value[1])
        print("总计\t%d" % total)
    db.close()


if __name__ == "__main__":
    main()
