/*moyq,更新url中参数的值
 * url 目标url 
 * arg 需要替换的参数名称 
 * arg_val 替换后的参数的值 
 * return url 参数替换后的url 
 */ 
 function changeURLArg(url,arg,arg_val){
     var pattern=arg+'=([^&]*)'; 
     var replaceText=arg+'='+arg_val; 
     if(url.match(pattern)){ 
         var tmp='/('+ arg+'=)([^&]*)/gi'; 
         tmp=url.replace(eval(tmp),replaceText); 
         return tmp; 
     }else{ 
         if(url.match('[\?]')){ 
             return url+'&'+replaceText; 
         }else{ 
             return url+'?'+replaceText; 
         } 
     } 
     return url+'\n'+arg+'\n'+arg_val; 
 }
 
 /**moyq
  * 数组去重
  */
 Array.prototype.unique = function()
 {
 	this.sort();
 	var re=[this[0]];
 	for(var i = 1; i < this.length; i++)
 	{
 		if( this[i] !== re[re.length-1])
 		{
 			re.push(this[i]);
 		}
 	}
 	return re;
 }
 
 /**
  * 获取浏览器版本信息
  */
 function getBrowserInfo() {
 	var agent = navigator.userAgent.toLowerCase();

 	var regStr_ie = /msie [\d.]+;/gi;
 	var regStr_ff = /firefox\/[\d.]+/gi
 	var regStr_chrome = /chrome\/[\d.]+/gi;
 	var regStr_saf = /safari\/[\d.]+/gi;
 	// IE
 	if (agent.indexOf("msie") > 0) {
 		return agent.match(regStr_ie);
 	}

 	// firefox
 	if (agent.indexOf("firefox") > 0) {
 		return agent.match(regStr_ff);
 	}

 	// Chrome
 	if (agent.indexOf("chrome") > 0) {
 		return agent.match(regStr_chrome);
 	}

 	// Safari
 	if (agent.indexOf("safari") > 0 && agent.indexOf("chrome") < 0) {
 		return agent.match(regStr_saf);
 	}
 }
 function setIfrmHeigth(id,minHeight) {
		var ifm = document.getElementById(id);
		var subWeb = document.frames ? document.frames[id].document
				: ifm.contentDocument;
		if (ifm != null && subWeb != null) {
			var browser = getBrowserInfo();
			var hh=Math.max(subWeb.documentElement.scrollHeight,subWeb.documentElement.clientHeight,subWeb.body.scrollHeight,minHeight);
			if(browser=='FF'){
				ifm.height = hh;
			}if(browser=='IE'){
				ifm.style.height=(hh+50)+'px';
				
			}if(browser=='Chrome'){
				ifm.height = hh;

			}else{
				ifm.height = hh;

			}
		}
	}