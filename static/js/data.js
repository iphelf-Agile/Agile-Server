function signalFilter(value, key, obj) {
    obj = $(obj);
    data = {};
    // Collect user input into data
    if(value!=null && key!=null && obj!=null){
        if (tempConstraint[key] == "" || tempConstraint[key] == value) {
            selected[key] = !selected[key];
        }
        if (selected[key]) {
            tempConstraint[key] = value;
            obj.parent().parent().find('a').removeClass('selected');
            obj.addClass('selected');
        } else {
            tempConstraint[key] = "";
            obj.removeClass('selected');
        }
        if (key == 2 || key == 4) {
            tempConstraint[key] = value;
        }
    }
    data["constraint"] = {};
    for (var i = 2; i <= 17; i++) {
        data["constraint"][fields[i]] = tempConstraint[i];
    }
    data["amount"] = 25;
    data["page"] = parseInt(page_num.value);
    data["sortby"] = "企业名称";
    $.ajax({
        type: "POST",
        url: "/filter",
        data: JSON.stringify(data),
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: slotFilter,
    });
}

function signalQuery() {
    data = {};
    // Collect user input into data
    data["entname"] = document.getElementById("search").value.split(",");
    data["amount"] = 25;
    data["page"] = parseInt(page_num.value);
    $.ajax({
        type: "POST",
        url: "/query",
        data: JSON.stringify(data),
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: slotQuery,
    });
}

function signalTrain() {
    data = {};
    // Collect user input into data
    $.ajax({
        type: "POST",
        url: "/train",
        data: JSON.stringify(data),
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: slotTrain,
    });
}

function signalPredict(){
    data={};
    // Collect user input into data
    for(var i=0;i<=36;i++)       //将key 和 value存入data
        data[A[i]]=B[i]
    $.ajax({
        type: 'POST',
        url: '/predict',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=UTF-8',
        dataType: 'json',
        success: slotPredict
    })
}

function slotFilter(data) {
    // Put data onto web page
    $('#dataTable').html("");
    var tableStr1 = "", tempDatas = data["data"], tempTitles = data["source"];
    for (var i = 0; i < tempDatas.length; i++) {
        tableStr1+="<tr>"
        for(let j=0;j<10;j++){
            if(tempDatas[i][j]==null) tempDatas[i][j]=""
            tableStr1+="<td align='center'>" + tempDatas[i][j] + "</td>"
        }
        for(let j=10;j<tempDatas[i].length;j++){
            if(tempDatas[i][j]==null) tempDatas[i][j]=""
            if(tempTitles[i][j-10]==null) tempTitles[i][j-10]="0"
            else tempTitles[i][j-10]="参与该标签的企业信息："+tempTitles[i][j-10]+"条"
            tableStr1+="<td align='center' title='" + tempTitles[i][j-10] + "'>" + tempDatas[i][j] + "</td>"
        }
        tableStr1+="</tr>"
    }
    $("#dataTable").html(tableStr1);
}

function slotQuery(data) {
    // Put data onto web page
    $('#dataTable').html("");
    var tableStr1 = "", tempDatas = data["data"];
    max_page=data["total"]
    page_num.max=data["total"]
    page_num.value=data["current"]
//    for (let i = 0; i < 25; i++) {
//        if (name != "") {
//            for (let j = 0; j < name.length; j++) {
//                if (datas[i][1] == name[j]) {
//                    tempDatas.push(datas[i]);
//                }
//            }
//        } else {
//            tempDatas.push(datas[i]);
//        }
//    }
    for (var i = 0; i < tempDatas.length; i++) {
        tableStr1+="<tr>"
        for(let j=0;j<tempDatas[i].length;j++){
            if(tempDatas[i][j]==null) tempDatas[i][j]=""
            tableStr1+="<td align='center'>" + tempDatas[i][j] + "</td>"
        }
        tableStr1+="</tr>"
    }
    if (tempDatas.length == 0) {
        tableStr1 = "<td colspan='18'><h1>未查询到此项，请前往预测企业界面进一步查看</h1></td>"
    }
    $("#dataTable").html(tableStr1);
}

function slotTrain(data){
    // Put data onto web page 管理员界面
    /* var Keys=new Array("企业信用-信用危机","企业信用-信用水平","企业稳定性-企业能力","企业稳定性-稳定水平", */
    /*                     "企业规模背景-企业年龄","企业规模背景-企业规模","企业风险-司法风险","企业风险-经营风险")
       Keys数组存放的是评估参数的类型，如上所示，定义在admin.html中 */
    var Tra_out=Array(12);
    for(let i=0;i<12;i++)
        Tra_out[i]=document.getElementById("adm_table").rows[i+1].cells;
    for(var i=1;i<=8;i++) for(var j=0;j<12;j++){
        n=data[Keys[i-1]][j];
        s=n.toPrecision(10);
        if(s.length>11) s=Number(s).toFixed(10-Number(s).toFixed(0).length);
        Tra_out[j][i].innerHTML=s;
    }
    window.clearInterval(timer);
}

function slotPredict(data){
    // Put data onto web page 预测界面
    var Pre_out=document.getElementById("pre_table").rows[1].cells
    for (var i=0;i<16;i++)
        Pre_out[i].innerHTML=data[i]      //将获取的data输出
}
